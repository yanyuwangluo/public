/**
* @author 烟雨
* @create_at 2022-09-07 18:50:07
* @title 买家秀
* @platform qq wx tg pgm web
* @rule 买家秀
* @priority 100
 * @public false
* @description 买家秀，少儿不宜。
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//烟雨阁
//https://www.yanyuwangluo.cn


const s = sender

var { body } = request({
    url: "https://api.vvhan.com/api/tao?type=json",
    method: "get",
    dataType: "json", //指定数据类型
})
 s.reply(`${body.title}${image(body.pic)}`)

//控制台打印log
//console.log(`${body.title}`)
