/**
* @author 烟雨
* @update onz3v
* @create_at 2023-01-11 15:03:06
* @title 抖音解析
* @platform qq wx tg pgm web
* @rule 解析 ?
* @priority 100
 * @public false
* @description 全新版抖音解析，原生接口
* @version v1.1.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/
// 固数 勿动
const headers = {
	"User-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36"
}
const api = 'https://www.iesdouyin.com/aweme/v1/web/aweme/detail/?aweme_id='
const params = '&aid=1128&version_name=23.5.0&device_platform=android&os_version=2333&Github=Evil0ctal&words=FXXK_U_ByteDance'
const s = sender
const address = s.param(1)
// ***********
linkParse(address)
// 解析链接获取url
function linkParse(params) {
	let reg = /https:\/\/v\.douyin\.com\/(\w+)?/g;
	main(reg.exec(params)[0])
}
// 获取视频id
function getAwemeId(url) {
	let { body } = request({
		url,
		allowredirects: false, //不禁止重定向
	});
	let reg = /video\/(\d+)?/g;
	let aweme_id = reg.exec(body)[1];
	console.log('获取到aweme_id', aweme_id)
	return aweme_id
}
// 主方法
function main(url) {
	console.log(api + getAwemeId(url) + params)
	let { body } = request({ url: api + getAwemeId(url) + params, headers });
	let data = JSON.parse(body)
	let { video, music, images } = data['aweme_detail'];
	let music_url = music.play_url.uri;
	let title = data.aweme_detail.desc;
	// 图文解析
	if (images && images.length) {
		s.reply(`标题：${title}\n音乐：${music_url}\n`)
		images.map(item => s.reply(image(item.url_list)))
	} else
		// 视频解析
		if (video) {
			let { bit_rate } = video;
			// 默认最高画质
			let index = getMax(bit_rate, 'quality_type');
			let { uri } = video['bit_rate'][index]['play_addr'];
			// let video_url = main_result_list[random(0, main_result_list.length)];
			let video_api = "https://aweme.snssdk.com/aweme/v1/play/?video_id=" + uri + "&ratio=720p&line=0"
			let { body } = request({ url: video_api, allowredirects: false });
			let video_url = body.match(/href=\"(\S*?)\"/)[1]
			s.reply("标题：" + title + "\n视频：" + video_url);
		}
}
// util
function getMax(arr, key) {
	var max = arr[0][key];
	var index = 0;
	for (var i = 1; i < arr.length; i++) {
		if (arr[i][key] > max) {
			max = arr[i][key];
			index = i;
		}
	}
	return index;
}