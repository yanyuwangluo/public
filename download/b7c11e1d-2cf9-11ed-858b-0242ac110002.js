/**
* @author 烟雨
* @create_at 2022-09-08 15:35:07
* @description 🐒XM刷步
* @title 刷步
* @platform qq wx tg pgm web cron

* @priority 100
* @admin false
 * @public false
* @description 修改小米运动步数，自行绑定
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//刷步 手机号 密码 步数 

//sender
//* @rule ^刷步 (1\d{10}) (\w+) (\d{1,5})$
//* @rule ^刷步$
const s = sender
const sillyGirl = new SillyGirl()


var chatID = `${s.getChatId()}` //获取当前群号
var userID = `${s.getUserId()}` //获取操作者QQ
if (chatID == "0") { //判定为私聊
    if (userID == "") { //判定为定时任务
        var phone = "" //内置账号
        var pwd = "" //内置密码
        var min = 20000,
            //最小步数
            max = 25000; //最大步数
        var steps = Math.floor(Math.random() * (max - min) + min); //随机步数
        var database = request({ // 内置http请求函数
            "url": "http://mi.yanyuwangluo.cn/MiStepApi?user=" + phone + "&password=" + pwd + "&step=" + steps,
            //"url"："http://wmy.avdgw.com/api/mi.php?phone=" + phone + "&password=" + pwd + "&steps=" + steps,
            //请求链接
            "method": "get",
            //请求方法
            "dataType": "json",
            //指定json类型数据
        })
        var msg = database.body.msg
        notifyMasters(msg) //通知管理员刷步情况
        console.log(database)
    } else {

        var phone = s.param(1) //匹配的账号
        var pwd = s.param(2) //匹配的密码
        var steps = s.param(3) //匹配的步数
        var xian = "100" //设定一个值，当步数小于此值时随机步数
        if (phone == "") {
            s.reply("参数不全或格式错误\n请输入:刷步 小米运动账号 小米运动密码 步数\n例如：刷步 15888888888 Abc123456 6666\n注：账号只支持手机号码，步数输入小于100时随机30000~90000步！")
        } else {
            if (parseInt(xian) > parseInt(steps)) {
                var min = 20000,
                    //最小步数
                    max = 25000; //最大步数
                var steps = Math.floor(Math.random() * (max - min) + min); //随机步数
                var database = request({ // 内置http请求函数
                    "url": "http://mi.yanyuwangluo.cn/MiStepApi?user=" + phone + "&password=" + pwd + "&step=" + steps,
                    //"url"："http://wmy.avdgw.com/api/mi.php?phone=" + phone + "&password=" + pwd + "&steps=" + steps,
                    //请求链接
                    "method": "get",
                    //请求方法
                    "dataType": "json",
                    //指定json类型数据
                })
                var msg = database.body.msg
                s.reply(msg) //通知操作者
                console.log(database)
            } else {
                 steps = s.param(3) //匹配的步数
                var database = request({ // 内置http请求函数
                    "url": "http://mi.yanyuwangluo.cn/MiStepApi?user=" + phone + "&password=" + pwd + "&step=" + steps,
                    //"url"："http://wmy.avdgw.com/api/mi.php?phone=" + phone + "&password=" + pwd + "&steps=" + steps,	
                    //请求链接
                    "method": "get",
                    //请求方法
                    "dataType": "json",
                    //指定json类型数据
                })
                var msg = database.body.msg
                s.reply(msg) //通知操作者
                console.log(database)
            }
        }
    }
} else {
    s.reply("为了保护你的隐私，请私聊") //在群发起时回复
}
