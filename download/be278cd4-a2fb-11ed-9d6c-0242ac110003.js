/**
* @author 烟雨
* @create_at 2023-02-19 17:01:47
* @title 文字转语音
* @platform qq wx tg pgm web
* @rule 文字转语音 ?
* @rule 转语音 ?
* @priority 100
 * @public false
* @description 文字转语音
* @version v1.1.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/
const s = sender
const text = s.param(1)

//request 网络请求
var { body } = request({
    url: "https://api.yanyuwangluo.cn/api/yuyin/?&text=" + text,
    method: "get",
    JSON: true
})
var mp3 = body["tts"]
s.reply(mp3)