/**
* @author 烟雨
* @create_at 2022-09-08 16:05:49
* @title 油价查询
* @rule ?油价
* @rule -youjia ?
* @priority 100
 * @public false
* @description 🌲查询地区油价，支持省份查询（看注释！！！）
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//先去天行api注册，然后申请油价接口。
//然后给机器人发 set tianapi key xxxxx


const s = sender
const address = s.param(1)
const sillyGirl = new SillyGirl()
const tianapiKey = new Bucket("tianapi").get("key")
if (!tianapiKey) {
    s.reply("请前往 www.tianapi.com 申请apiKey。\n并使用命令 set tianapi key xxxx 写入key")
} else {
var { body } = request({
    url: "http://api.tianapi.com/oilprice/index?key=" + tianapiKey + "&prov=" + address, //请求链接
    method: "get",
    json: true,
});
if (body.code == 200) {
    var list = body.newslist[0];
    data =
        "查询地区：" +
        list.prov +
        "\n零号柴油：" +
        list.p0 +
        "\n89号汽油：" +
        list.p89 +
        "\n92号汽油：" +
        list.p92 +
        "\n95号汽油：" +
        list.p95 +
        "\n98号汽油：" +
        list.p98 +
        "\n更新时间：" +
        list.time.slice(0,10);
} else {
    data = body.msg;
}

s.reply(data)
}