/**
* @author 烟雨
* @create_at 2022-09-07 13:58:20
* @title 图片解析
* @platform qq wx tg pgm web
* @rule 图片解析 ?
* @priority 100
 * @public false
* @description 抖音图片解析。
* @version v1.1.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/


//烟雨阁
//https://www.yanyuwangluo.cn

const getVarType = (function (o) { return (Object.prototype.toString.call(o).match(/\[object (.*?)\]/) || [])[1].toLowerCase(); });
//sender
const s = sender
const sillyGirl = new SillyGirl()
const kw = s.param(1)
//正则取值
var ts = httpString(kw)[0];
const url = "https://www.hmily.vip/api/dy/?url=" + ts;
//console.log(url)
//请求链接
var { body } = request({
	url,
	method: "get",
	dataType: "json"
})

if (body.status == 200) {
	let result = body
	const title = result.desc;
	const videoUrl = result.video_url
		// 图片
		s.reply(`标题：${title}\n`)
		for (let i in videoUrl) {
			s.reply(image(videoUrl[i]))
		}
} else {
	s.reply("解析失败，请复制抖音链接重试。")
}


//正则
function httpString(s) {
	var reg = /(https?|http|ftp|file):\/\/[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g
	s = s.match(reg)
	return s
}