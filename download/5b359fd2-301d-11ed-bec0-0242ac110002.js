/**
* @author 烟雨
* @create_at 2022-09-09 17:05:17
* @description 🐒修复旧版本值得买插件。
* @version v1.0.0
* @title 值得买
* @platform qq wx tg pgm web cron
* @rule 值得买 ?
* @priority 100
 * @public false
* @description 修复旧版本值得买插件
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//请在消息框输入并发送：你好 佩奇
//建议同时打开浏览器控制台

//sender
const s = sender
  
var key = encodeURI(s.param(1))
option = {
    "method": "get",
    "url": `https://search.smzdm.com/?c=faxian&s=${key}&order=time&v=b`,
}

request(option, (error, response, body) => {
    body = body.replace(/\n/g, "")
    var result = body.match(/<div class="feed-link-btn-inner">(.*?)<\/div>/g)
    var goods = []
    for (let index = 0; index < result.length; index++) {

        var name = result[index].match(/'name':'(.*?)'/)[1]
        var price = result[index].match(/'price':(\d+),/)[1]
        var link = result[index].match(/href="(.*?)"/)[1]
        goods.push(`${index+1}. ${name}\n￥${price}\n${link}`)
    }
    s.reply(`为您找到${goods.length}个商品：` + "\n" + goods.join("\n"))
})