/**
* @author 烟雨
* @create_at 2022-09-07 18:49:11
* @title 登录提示
* @platform qq wx tg pgm web
* @rule 登录
* @priority 999
 * @public false
* @description 修改登录提示
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//烟雨阁
//https://www.yanyuwangluo.cn

const s = sender

var ts = "登录已关闭，欢迎打赏"

s.reply(image("https://tu.yanyuwangluo.cn/2021/12/27/8fc8481faa766.jpg") + ts)