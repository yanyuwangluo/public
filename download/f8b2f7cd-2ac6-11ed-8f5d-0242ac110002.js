/**
* @author 烟雨
 * @version v1.0.0
* @create_at 2022-09-07 14:14:33
* @description 🐒获取群信息
* @title test
* @platform qq wx tg pgm web
* @rule test
* @rule 群信息
 * @public false
* @priority 100
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/


//烟雨阁
//https://www.yanyuwangluo.cn


//sender
const s = sender
//sillyGirl
const sillyGirl = new SillyGirl()

s.reply(`消息平台：${s.getPlatform()}
        \n用户ID：${s.getUserId()}
        \n群聊ID：${s.getChatId()}
        \n用户名称：${s.getUsername()}
        \n消息内容：${s.getContent()}`)
