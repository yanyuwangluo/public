/**
* @author 烟雨
 * @create_at 2022-10-21 20:20:24
 * @description 🐒这个人很懒什么都没有留下。
 * @version v1.0.0
* @title 口令解析
* @platform qq wx  web
* @rule 口令解析 ?
* @rule 膨胀解析 ?
* @rule 解析膨胀 ?
* @rule 解析口令 ?
* @rule code ?
* @rule code?
* @priority 100
 * @public false
* @description 解析口令（兰兰接口）
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/

//请在消息框输入并发送：你好 佩奇
//建议同时打开浏览器控制台

//sender
const s = sender

//sender param 获取第1个参数
const code = s.param(1)
//request 网络请求
var _data = {"code": code}
var jiexi = request({
    url: 'https://api.nolanstore.top/JComExchange',
    method: 'POST',
    dataType:'json',
    headers: {
        "content-type": "application/json",
    },
    body: _data
},function(err, resp, data) {
    if (!err && resp.statusCode == 200) {
     if(data){
         var str = `${data.data.jumpUrl}`
	var reg_g = 
	/(?<=inviteId=)(.+?)(?=&)/g;
	var result = str.match(reg_g);
	 s.reply(`${image(data.data.img)}`+"分享来自："+data.data.userName + "\n链接："+ data.data.jumpUrl+"\n助力码:"+result)}
    }else{
      s.reply("接口挂了，联系烟雨")
     }
});