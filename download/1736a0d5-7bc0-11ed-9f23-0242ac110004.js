/**
 * @title 老黄历
 * @create_at 2022-12-18 16:43:35
 * @rule 老黄历
 * @rule 黄历
 * @description 🐒这个人很懒什么都没有留下。
 * @author 烟雨
 * @version v1.0.0
 * @priority 100
 * @public false
* @description 老黄历
* @version v1.0.0
* @icon https://www.yanyuwangluo.cn/yanyu.ico
*/
const s = sender

var { body } = request({
    url: "https://xiaobai.klizi.cn/API/other/laohuangli.php",
    method: "get",
    allowredirects: false, //不禁止重定向
})
// console.log(body)
s.reply(body)